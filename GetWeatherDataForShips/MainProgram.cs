﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Collections;

using System.Web;

using VEEMSOffice.Components.Web;
using VEEMSOffice.Components.Business;
using System.Globalization;
using System.IO;

using System.Xml;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;


using Newtonsoft.Json;

namespace GetWeatherDataForShips
{
    class MainProgram
    {
        static void Main(string[] args)
        {
            //PageMessage.Text = String.Empty;

            //tbGenQuery.Text = GetWeatherData("13.0827","80.2707");

            string strGetCurrentDate = String.Empty;
            strGetCurrentDate = String.Format("{0:dd-MMM-yyyy HH:mm:ss}", DateTime.UtcNow);

            MainProgram objMainProgram1 = new MainProgram();

            objMainProgram1.TraceService("WeatherDataService Started : " + strGetCurrentDate + "(UTC) ");

            int i = 0;

            for (i = 0; i < 64000; i++ )
            {
                objMainProgram1.GetWeatherData(i, DateTime.Now, "95.0827", "80.2707"); 
            }
            

            //string strGetShipsForWeatherQuery = String.Empty;
            //strGetShipsForWeatherQuery = " select VEEMS_SHIP_ID, VEEMS_SHIP_NAME, VEEMS_ONLY_MRV_IND from WEATHER_DATA_SHIP_LIST order by VEEMS_SHIP_ID ";

            //DataSet dsShipsForWeatherQuery = new DataSet();


            //dsShipsForWeatherQuery = objMainProgram1.QuerySelect(strGetShipsForWeatherQuery);



            //foreach(DataRow drShipsForWeatherQuery in dsShipsForWeatherQuery.Tables[0].Rows)
            //{

            //    string strShipId = String.Empty;

            //    int intOnlyMrvInd = 0;


            //    strShipId = drShipsForWeatherQuery[0].ToString().Trim();

            //    intOnlyMrvInd = Convert.ToInt32(drShipsForWeatherQuery[2].ToString().Trim());

            //    MainProgram objMainProgram = new MainProgram();
            //    objMainProgram.GetShipInfo(strShipId, intOnlyMrvInd);

            //}
            //string[] strShipIdArray = new string[] { "1", "3", "11", "19", "29", "35" };

            //  string[] strShipIdArray = new string[] { "1" };


            //foreach (string strShipId in strShipIdArray)
            //{
            //    MainProgram objMainProgram = new MainProgram();
            //    objMainProgram.GetShipInfo(strShipId); 

            //}

            if (i == 4800)
            {
                Console.WriteLine("480 calls to the windy api");
            }

            //PageMessage.Text = "Weather Information Saved!";
            // GetWeatherData(1,DateTime.Now,"13.0827", "80.2707");

            strGetCurrentDate = String.Format("{0:dd-MMM-yyyy HH:mm:ss}", DateTime.UtcNow);
            objMainProgram1.TraceService("WeatherDataService Ended : " + strGetCurrentDate + "(UTC) ");
            objMainProgram1.TraceService("----------------------------------------------------------");
        }



        private void TraceService(string content)
        {

            FileStream fs = new FileStream("C:\\GetWeatherDataForShips\\TraceLogFile.txt", FileMode.OpenOrCreate, FileAccess.Write);

            StreamWriter sw = new StreamWriter(fs);

            sw.BaseStream.Seek(0, SeekOrigin.End);
            sw.WriteLine(content);

            sw.Flush();
            sw.Close();
        }

        private void GetShipInfo(string strShipId, int intOnlyMrvInd)
        {

            //string strShipTypeQuery = String.Empty;

            //strShipTypeQuery = "select VEEMS_CONFIG_VALUE from VEEMS_CONFIG where VEEMS_SHIP_ID = " + strShipId + " and VEEMS_CONFIG_KEY = 'ONLYMRVIND'";

            int intShipType = 0;

            //intShipType = QuerySelectRetInt(strShipTypeQuery);

            intShipType = intOnlyMrvInd;



            string strShipInfo = String.Empty;

            if (intShipType == 0) // Automatic Ship
            {
                strShipInfo += " select top 1 B.VEEMS_SHIP_ID, A.VEEMS_GPS_UTC_DATE_TIME, A.VEEMS_LATITUDE, A.VEEMS_LONGITUDE, A.VEEMS_SHIP_DATA_OFFICE_ID ";
                strShipInfo += " from VEEMS_SHIP_DATA_OFFICE A, VEEMS_SHIP B where  A.VEEMS_SHIP_IMO_NUMBER = B.VEEMS_IMO_NUMBER and  B.VEEMS_SHIP_ID = " + strShipId + " ";
                strShipInfo += " order by A.VEEMS_GPS_UTC_DATE_TIME desc ";
            }
            else // Manual Ship
            {
                strShipInfo += " select top 1 VEEMS_SHIP_ID, VEEMS_REPORT_UTC_END_DATE, VEEMS_REPORT_LATITUDE, VEEMS_REPORT_LONGITUDE, VEEMS_SHIP_REPORT_ID from VEEMS_SHIP_REPORT where VEEMS_SHIP_ID = " + strShipId + " ";
                strShipInfo += " and VEEMS_SHIP_REPORT_TYPE_ID in (1,2,3,4,5,6,7,8) ";
                strShipInfo += " order by VEEMS_REPORT_UTC_END_DATE desc ";

            }

            DataSet dsShipInfo = new DataSet();

            dsShipInfo = QuerySelect(strShipInfo);

            if ((dsShipInfo != null) && (dsShipInfo.Tables[0].Rows.Count > 0))
            {
                int intShipId = Convert.ToInt32(dsShipInfo.Tables[0].Rows[0][0].ToString().Trim());

                DateTime dtUtcDate = Convert.ToDateTime(dsShipInfo.Tables[0].Rows[0][1].ToString().Trim());

                double dblLatitude = Convert.ToDouble(dsShipInfo.Tables[0].Rows[0][2].ToString().Trim());

                double dblLongitude = Convert.ToDouble(dsShipInfo.Tables[0].Rows[0][3].ToString().Trim());

                int intPKey = Convert.ToInt32(dsShipInfo.Tables[0].Rows[0][4].ToString().Trim());

                if ((dblLatitude >= -90.0) && (dblLatitude <= 90.0) && (dblLongitude >= -180.0) && (dblLongitude <= 180.0))
                {

                    int intValid = 0;

                    intValid = WeatheDataExistCheck(intShipId, intPKey, intShipType);

                    if (intValid == 0)
                    {
                        string strOutput = GetWeatherData(intShipId, dtUtcDate, dblLatitude.ToString(), dblLongitude.ToString());

                        InsertWeatherDataMark(intShipId, intPKey, intShipType);

                    }


                    // File.WriteAllText("D:\\Exception.txt", strOutput);

                    //  ParseCSV(1, dblLatitude.ToString(), dblLongitude.ToString());

                }


            }


        }

        private int WeatheDataExistCheck(int intShipId, int intPKey, int intShipType)
        {
            int intOutput = 0;

            string strDataExist = String.Empty;
            strDataExist = " select count(1) from WEATHER_DATA_EXIST where VEEMS_SHIP_ID = " + intShipId.ToString().Trim() + " and VEEMS_PKEY = " + intPKey.ToString().Trim() + " and VEEMS_ONLY_MRV_IND = " + intShipType.ToString().Trim() + " ";

            intOutput = QuerySelectRetInt(strDataExist);


            return intOutput;
        }

        private int InsertWeatherDataMark(int intShipId, int intPKey, int intShipType)
        {
            int intOutput = 0;

            string strDataInsert = String.Empty;
            strDataInsert = " INSERT INTO WEATHER_DATA_EXIST(VEEMS_SHIP_ID,VEEMS_PKEY,VEEMS_ONLY_MRV_IND,VEEMS_UTC_DATE_TIME)VALUES(" + intShipId.ToString().Trim() + "," + intPKey.ToString().Trim() + "," + intShipType.ToString().Trim() + ",'" + String.Format("{0:dd-MMM-yyyy HH:mm:ss}", DateTime.UtcNow) + "') ";

            intOutput = QueryExecute(strDataInsert);


            return intOutput;
        }


        //
        private string GetWeatherData(int intShipId, DateTime dtUtcTime, string strLat, string strLong)        
        {

            string strInputString = String.Empty;

                

            strInputString += "https://node.windy.com/forecast/v2.1/ecmwfWaves/" + "1.279467" + "/" + "-103.940467" + "";

            try
            {


                // Create a request for the URL.   
                WebRequest request = WebRequest.Create(strInputString);
                // If required by the server, set the credentials.  
                request.Credentials = CredentialCache.DefaultCredentials;
                // Get the response.  
                WebResponse response = request.GetResponse();
                // Display the status.  
                Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                // Get the stream containing content returned by the server.  
                Stream dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.  
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.  
                string responseFromServer = reader.ReadToEnd();
                // Display the content.  
                // Console.WriteLine(responseFromServer);

                //tbOutput.Text = responseFromServer;

                if (((HttpWebResponse)response).StatusDescription != "OK")
                {
                    Console.WriteLine("errored at the hit count" + intShipId);
                }

                // Clean up the streams and the response.  
                reader.Close();
                response.Close();

                Console.WriteLine("Invoking api count" + intShipId);
                string strOutput = "test";
                    //ParseXml(responseFromServer, intShipId, dtUtcTime, strLat, strLong);

                // tbGenQuery.Text = responseFromServer;

                //string strOutput = responseFromServer;
                return strOutput;

            }
            catch (WebException e)
            {
                return "Error code: " + e.Status;
            }
            catch (Exception e)
            {
                return "The following Exception was raised : " + e.Message;
            }



        }

        private string ParseXml(string strInput, int intShipId, DateTime dtUtcTime, string strLat, string strLong)
        {
            string strOutput = String.Empty;


            try
            {
                // XmlDocument xdoc = new XmlDocument();
                // xdoc.LoadXml(strInput);


                //String strJsonContent = File.ReadAllText("D:\\DemoData.json");

                //XmlDocument doc = JsonConvert.DeserializeXmlNode(strInput);

                String strJsonContent = strInput;

                XmlDocument doc = JsonConvert.DeserializeXmlNode(strJsonContent, "root");

                string strDestDirName = "C:\\TempXmlDir";
                //if (Directory.Exists(strDestDirName))
                //{
                //    Directory.Delete(strDestDirName, true);
                //}

                if (!Directory.Exists(strDestDirName))
                {
                    Directory.CreateDirectory(strDestDirName);
                }


                doc.Save("C:\\TempXmlDir\\temp.xml");



                string xmlcontents = File.ReadAllText("C:\\TempXmlDir\\temp.xml");

                int intFound = 0;

                int intDateInd = 0;

                int intHourInd = 0;



                //   DateTime dtNow = Convert.ToDateTime(GetDateTimeFromCoordinates());

                DateTime dtNow = DateTime.Now;

                string strCurrentDate = String.Format("{0:yyyy-MM-dd}", dtNow);

                //  return strCurrentDate;

                string strNewLine = Environment.NewLine;

                string strDayInfo = String.Empty;

                string strPattern = String.Empty;

                string strMainString = String.Empty;

                string strLatLongInfo = String.Empty;

                string strShipId = String.Empty;

                //strShipId = tbShipId.Text.Trim();


                int intHeaderInd = 0;

                int intHeaderQueryInd = 0;

                int intHeaderPKey = 0;



                int intCelestialInd = 0;

                int intSummaryInd = 0;

                int IntDataInd = 0;

                //using (XmlReader reader = XmlReader.Create(new StringReader(strInput)))


                using (XmlReader reader = XmlReader.Create("C:\\TempXmlDir\\temp.xml"))
                {
                    //PageMessage.Text = "1";

                    //string strMainString2 = String.Empty;
                    //while (reader.Read())
                    //{
                    //    if (reader.IsStartElement())
                    //    {
                    //       // strMainString2 += reader.Name + "\n";

                    //        File.AppendAllText("D:\\XMLHeaders.txt", reader.Name + "\n");

                    //    }


                    //}

                    //File.WriteAllText("C:\\XMLHeaders.txt", strMainString2);


                    //return "";

                    string strHeaderQuery = String.Empty;
                    strHeaderQuery = " INSERT INTO WindyForcastDataHeader(ShipId,UtcTimeFromVof,Latitude,Longitude,model,refTime,update_Time,updateTs,elevation,origElevation,origLat,origLon,step,utcOffset, ";
                    strHeaderQuery += " tzName,sunset,sunrise,hasWaves,cache,TZname2,TZ_offset,TZoffsetFormatted,TZatNearest,nowObserved,night,sunset2,sunrise2,dusk,sunsetTs,sunriseTs,duskTs,atSea,isDay, ";
                    strHeaderQuery += " VEEMS_DELETED_FLAG,VEEMS_CREATED_BY,VEEMS_CREATED_DATE,VEEMS_MODIFIED_BY,VEEMS_MODIFIED_DATE) ";
                    strHeaderQuery += " VALUES ";
                    strHeaderQuery += " (" + intShipId.ToString().Trim() + " ";
                    strHeaderQuery += " ,'" + String.Format("{0:dd-MMM-yyyy HH:mm:ss}", dtUtcTime) + "'";
                    strHeaderQuery += " ," + strLat + " ";
                    strHeaderQuery += " ," + strLong + "";


                    string strSummaryQueryPattern = String.Empty;

                    strSummaryQueryPattern += " INSERT INTO  WindyForcastDataSummary(WindyForcastDataHeader_ID,icon,date_Info,indexInfo,time_stamp,week_day,day_Info,tempMax,tempMin,wind,windDir,segments,icon2, ";
                    strSummaryQueryPattern += " VEEMS_DELETED_FLAG,VEEMS_CREATED_BY,VEEMS_CREATED_DATE,VEEMS_MODIFIED_BY,VEEMS_MODIFIED_DATE) ";
                    strSummaryQueryPattern += " VALUES(";


                    string strDataQueryPattern = String.Empty;
                    strDataQueryPattern += " INSERT INTO WindyForcastDataDetails(WindyForcastDataHeader_ID,day_Info,hour_Info,ts,origTs,isDay,origDate_Info,temp,dewPoint,cbase,icon,icon2,weathercode,mm,snowPrecip, ";
                    strDataQueryPattern += " convPrecip,rain,snow,pressure,wind,windDir,rh,gust,waves,wavesDir,wavesPeriod,swell1,swell1Dir,swell1Period,swell2,swell2Dir,swell2Period,swell,swellDir,swellPeriod, ";
                    strDataQueryPattern += " VEEMS_DELETED_FLAG,VEEMS_CREATED_BY,VEEMS_CREATED_DATE,VEEMS_MODIFIED_BY,VEEMS_MODIFIED_DATE) ";
                    strDataQueryPattern += " VALUES ";
                    strDataQueryPattern += " ( ";


                    string strSummaryQuery = String.Empty;

                    string strDataQuery = String.Empty;


                    List<string> lstday = new List<string>();
                    List<string> lsthour = new List<string>();
                    List<string> lstts = new List<string>();
                    List<string> lstorigTs = new List<string>();
                    List<string> lstisDay = new List<string>();
                    List<string> lstorigDate = new List<string>();
                    List<string> lsttemp = new List<string>();
                    List<string> lstdewPoint = new List<string>();
                    List<string> lstcbase = new List<string>();
                    List<string> lsticon = new List<string>();
                    List<string> lsticon2 = new List<string>();
                    List<string> lstweathercode = new List<string>();
                    List<string> lstmm = new List<string>();
                    List<string> lstsnowPrecip = new List<string>();
                    List<string> lstconvPrecip = new List<string>();
                    List<string> lstrain = new List<string>();
                    List<string> lstsnow = new List<string>();
                    List<string> lstpressure = new List<string>();
                    List<string> lstwind = new List<string>();
                    List<string> lstwindDir = new List<string>();
                    List<string> lstrh = new List<string>();
                    List<string> lstgust = new List<string>();
                    List<string> lstwaves = new List<string>();
                    List<string> lstwavesDir = new List<string>();
                    List<string> lstwavesPeriod = new List<string>();
                    List<string> lstswell1 = new List<string>();
                    List<string> lstswell1Dir = new List<string>();
                    List<string> lstswell1Period = new List<string>();
                    List<string> lstswell2 = new List<string>();
                    List<string> lstswell2Dir = new List<string>();
                    List<string> lstswell2Period = new List<string>();
                    List<string> lstswell = new List<string>();
                    List<string> lstswellDir = new List<string>();
                    List<string> lstswellPeriod = new List<string>();





                    while (reader.Read())
                    {
                        if (reader.IsStartElement())
                        {
                            if (reader.Name == "header")
                            {
                                if (reader.Read())
                                {
                                    intHeaderInd = 1;
                                }
                            }
                        }

                        if (intHeaderInd == 1)
                        {

                            if (reader.IsStartElement())
                            {
                                if ((reader.Name == "model"))
                                {
                                    if (reader.Read())
                                    {

                                        strHeaderQuery += ",'" + reader.Value.Trim() + "'";

                                    }

                                }
                            }
                            if (reader.IsStartElement())
                            {
                                if ((reader.Name == "refTime"))
                                {
                                    if (reader.Read())
                                    {
                                        strHeaderQuery += ",'" + String.Format("{0:dd-MMM-yyyy HH:mm:ss}", Convert.ToDateTime(reader.Value.Trim())) + "'";

                                    }

                                }
                            }
                            if (reader.IsStartElement())
                            {
                                if ((reader.Name == "update"))
                                {
                                    if (reader.Read())
                                    {
                                        strHeaderQuery += ",'" + String.Format("{0:dd-MMM-yyyy HH:mm:ss}", Convert.ToDateTime(reader.Value.Trim())) + "'";

                                    }

                                }
                            }

                            if (reader.IsStartElement())
                            {
                                if (reader.Name == "updateTs")
                                {
                                    if (reader.Read())
                                    {

                                        strHeaderQuery += ",'" + reader.Value.Trim() + "'";

                                    }

                                }
                                if (reader.Name == "elevation")
                                {
                                    if (reader.Read())
                                    {

                                        strHeaderQuery += ",'" + reader.Value.Trim() + "'";

                                    }

                                }
                                if (reader.Name == "origElevation")
                                {
                                    if (reader.Read())
                                    {
                                        strHeaderQuery += ",'" + reader.Value.Trim() + "'";

                                    }

                                }
                                if (reader.Name == "origLat")
                                {
                                    if (reader.Read())
                                    {

                                        strHeaderQuery += ",'" + reader.Value.Trim() + "'";

                                    }

                                }
                                if (reader.Name == "origLon")
                                {
                                    if (reader.Read())
                                    {
                                        strHeaderQuery += ",'" + reader.Value.Trim() + "'";

                                    }

                                }
                                if (reader.Name == "step")
                                {
                                    if (reader.Read())
                                    {
                                        strHeaderQuery += ",'" + reader.Value.Trim() + "'";

                                    }

                                }
                                if (reader.Name == "utcOffset")
                                {
                                    if (reader.Read())
                                    {
                                        strHeaderQuery += ",'" + reader.Value.Trim() + "'";

                                    }

                                }
                                if (reader.Name == "tzName")
                                {
                                    if (reader.Read())
                                    {

                                        strHeaderQuery += ",'" + reader.Value.Trim() + "'";

                                    }

                                }
                                if (reader.Name == "sunset")
                                {
                                    if (reader.Read())
                                    {
                                        strHeaderQuery += ",'" + reader.Value.Trim() + "'";

                                    }

                                }
                                if (reader.Name == "sunrise")
                                {
                                    if (reader.Read())
                                    {

                                        strHeaderQuery += ",'" + reader.Value.Trim() + "'";

                                    }

                                }
                                if (reader.Name == "hasWaves")
                                {
                                    if (reader.Read())
                                    {


                                        // 

                                        strHeaderQuery += ",'" + reader.Value.Trim() + "'";

                                    }

                                }
                                if (reader.Name == "cache")
                                {
                                    if (reader.Read())
                                    {

                                        strHeaderQuery += ",'" + reader.Value.Trim() + "'";

                                    }

                                }



                            }


                            if (reader.IsStartElement())
                            {
                                if (reader.Name == "celestial")
                                {
                                    if (reader.Read())
                                    {
                                        intCelestialInd = 1;

                                    }

                                }
                            }


                            if (intCelestialInd == 1)
                            {
                                if (reader.IsStartElement())
                                {
                                    if (reader.Name == "TZname")
                                    {
                                        if (reader.Read())
                                        {
                                            strHeaderQuery += ",'" + reader.Value.Trim() + "'";

                                        }

                                    }
                                    if (reader.Name == "TZoffset")
                                    {
                                        if (reader.Read())
                                        {
                                            strHeaderQuery += ",'" + reader.Value.Trim() + "'";

                                        }

                                    }
                                    if (reader.Name == "TZoffsetFormatted")
                                    {
                                        if (reader.Read())
                                        {
                                            strHeaderQuery += ",'" + reader.Value.Trim() + "'";

                                        }

                                    }

                                    if (reader.Name == "TZatNearest")
                                    {
                                        if (reader.Read())
                                        {

                                            strHeaderQuery += ",'" + reader.Value.Trim() + "'";

                                        }

                                    }

                                    if (reader.Name == "nowObserved")
                                    {
                                        if (reader.Read())
                                        {
                                            strHeaderQuery += ",'" + String.Format("{0:dd-MMM-yyyy HH:mm:ss}", Convert.ToDateTime(reader.Value.Trim())) + "'";

                                        }

                                    }
                                    if (reader.Name == "night")
                                    {
                                        if (reader.Read())
                                        {
                                            strHeaderQuery += ",'" + String.Format("{0:dd-MMM-yyyy HH:mm:ss}", Convert.ToDateTime(reader.Value.Trim())) + "'";

                                        }

                                    }



                                    if (reader.Name == "sunset")
                                    {
                                        if (reader.Read())
                                        {
                                            strHeaderQuery += ",'" + reader.Value.Trim() + "'";

                                        }

                                    }
                                    if (reader.Name == "sunrise")
                                    {
                                        if (reader.Read())
                                        {
                                            strHeaderQuery += ",'" + reader.Value.Trim() + "'";

                                        }

                                    }
                                    if (reader.Name == "dusk")
                                    {
                                        if (reader.Read())
                                        {
                                            strHeaderQuery += ",'" + reader.Value.Trim() + "'";

                                        }

                                    }
                                    if (reader.Name == "sunsetTs")
                                    {
                                        if (reader.Read())
                                        {
                                            strHeaderQuery += ",'" + reader.Value.Trim() + "'";

                                        }

                                    }
                                    if (reader.Name == "sunriseTs")
                                    {
                                        if (reader.Read())
                                        {
                                            strHeaderQuery += ",'" + reader.Value.Trim() + "'";

                                        }

                                    }
                                    if (reader.Name == "duskTs")
                                    {
                                        if (reader.Read())
                                        {

                                            strHeaderQuery += ",'" + reader.Value.Trim() + "'";

                                        }

                                    }
                                    if (reader.Name == "atSea")
                                    {
                                        if (reader.Read())
                                        {


                                            strHeaderQuery += ",'" + reader.Value.Trim() + "'";

                                        }

                                    }
                                    if (reader.Name == "isDay")
                                    {
                                        if (reader.Read())
                                        {
                                            strHeaderQuery += ",'" + reader.Value.Trim() + "'";

                                        }
                                        intHeaderInd = 0;
                                        intHeaderQueryInd = 1;
                                    }





                                }

                            }


                            if ((intHeaderQueryInd == 1) && (intHeaderPKey == 0))
                            {
                                //ExecQuery



                                strHeaderQuery += " ,0,'System',GetDate(),'System',GetDate()) ";



                                int intReturn = QueryExecute(strHeaderQuery);



                                intHeaderQueryInd = 0;



                                // File.WriteAllText("D:\\TestQuery.txt", strHeaderQuery);

                                if (intReturn == 0)
                                {


                                    string strHeaderPKey = String.Empty;
                                    strHeaderPKey += " select max(WindyForcastDataHeader_ID)  from WindyForcastDataHeader ";
                                    strHeaderPKey += " where ShipId = " + intShipId.ToString().Trim() + " and UtcTimeFromVOf = '" + String.Format("{0:dd-MMM-yyyy HH:mm:ss}", dtUtcTime) + "' and Latitude = " + strLat + "  and Longitude = " + strLong + " ";

                                    intHeaderPKey = QuerySelectRetInt(strHeaderPKey);


                                }


                            }



                        }

                        if (intHeaderPKey > 0)
                        {
                            if (reader.IsStartElement())
                            {
                                if (reader.Name == "summary")
                                {
                                    if (reader.Read())
                                    {
                                        intSummaryInd = 1;

                                    }
                                }
                            }
                            if (reader.IsStartElement())
                            {
                                if (intSummaryInd == 1)
                                {
                                    if (reader.Name == "icon")
                                    {
                                        if (reader.Read())
                                        {
                                            strSummaryQuery = intHeaderPKey.ToString().Trim();
                                            strSummaryQuery += ",'" + reader.Value.Trim() + "'";
                                        }
                                    }
                                    if (reader.Name == "date")
                                    {
                                        if (reader.Read())
                                        {
                                            strSummaryQuery += ",'" + String.Format("{0:dd-MMM-yyyy HH:mm:ss}", Convert.ToDateTime(reader.Value.Trim())) + "'";
                                        }
                                    }
                                    if (reader.Name == "index")
                                    {
                                        if (reader.Read())
                                        {
                                            strSummaryQuery += ",'" + reader.Value.Trim() + "'";
                                        }
                                    }
                                    if (reader.Name == "timestamp")
                                    {
                                        if (reader.Read())
                                        {
                                            strSummaryQuery += ",'" + reader.Value.Trim() + "'";
                                        }
                                    }
                                    if (reader.Name == "weekday")
                                    {
                                        if (reader.Read())
                                        {
                                            strSummaryQuery += ",'" + reader.Value.Trim() + "'";
                                        }
                                    }
                                    if (reader.Name == "day")
                                    {
                                        if (reader.Read())
                                        {
                                            strSummaryQuery += ",'" + reader.Value.Trim() + "'";
                                        }
                                    }
                                    if (reader.Name == "tempMax")
                                    {
                                        if (reader.Read())
                                        {
                                            strSummaryQuery += ",'" + reader.Value.Trim() + "'";
                                        }
                                    }
                                    if (reader.Name == "tempMin")
                                    {
                                        if (reader.Read())
                                        {
                                            strSummaryQuery += ",'" + reader.Value.Trim() + "'";
                                        }
                                    }
                                    if (reader.Name == "wind")
                                    {
                                        if (reader.Read())
                                        {
                                            strSummaryQuery += ",'" + reader.Value.Trim() + "'";
                                        }
                                    }
                                    if (reader.Name == "windDir")
                                    {
                                        if (reader.Read())
                                        {
                                            strSummaryQuery += ",'" + reader.Value.Trim() + "'";
                                        }
                                    }
                                    if (reader.Name == "segments")
                                    {
                                        if (reader.Read())
                                        {
                                            strSummaryQuery += ",'" + reader.Value.Trim() + "'";
                                        }
                                    }
                                    if (reader.Name == "icon2")
                                    {
                                        if (reader.Read())
                                        {
                                            strSummaryQuery += ",'" + reader.Value.Trim() + "'";

                                            QueryExecute(strSummaryQueryPattern + strSummaryQuery + " ,0,'System',GetDate(),'System',GetDate()) ");


                                        }
                                    }



                                }

                            }
                        }



                        if (intHeaderPKey > 0)
                        {
                            if (reader.IsStartElement())
                            {
                                if (reader.Name == "data")
                                {
                                    if (reader.Read())
                                    {
                                        intSummaryInd = 0;
                                        IntDataInd = 1;

                                    }
                                }
                            }
                            if (reader.IsStartElement())
                            {
                                if (IntDataInd == 1)
                                {
                                    if (reader.Name == "day") { if (reader.Read()) { lstday.Add(reader.Value.Trim()); } }
                                    if (reader.Name == "hour") { if (reader.Read()) { lsthour.Add(reader.Value.Trim()); } }
                                    if (reader.Name == "ts") { if (reader.Read()) { lstts.Add(reader.Value.Trim()); } }
                                    if (reader.Name == "origTs") { if (reader.Read()) { lstorigTs.Add(reader.Value.Trim()); } }
                                    if (reader.Name == "isDay") { if (reader.Read()) { lstisDay.Add(reader.Value.Trim()); } }
                                    if (reader.Name == "origDate") { if (reader.Read()) { lstorigDate.Add(reader.Value.Trim()); } }
                                    if (reader.Name == "temp") { if (reader.Read()) { lsttemp.Add(reader.Value.Trim()); } }
                                    if (reader.Name == "dewPoint") { if (reader.Read()) { lstdewPoint.Add(reader.Value.Trim()); } }
                                    if (reader.Name == "cbase") { if (reader.Read()) { lstcbase.Add(reader.Value.Trim()); } }
                                    if (reader.Name == "icon") { if (reader.Read()) { lsticon.Add(reader.Value.Trim()); } }
                                    if (reader.Name == "icon2") { if (reader.Read()) { lsticon2.Add(reader.Value.Trim()); } }
                                    if (reader.Name == "weathercode") { if (reader.Read()) { lstweathercode.Add(reader.Value.Trim()); } }
                                    if (reader.Name == "mm") { if (reader.Read()) { lstmm.Add(reader.Value.Trim()); } }
                                    if (reader.Name == "snowPrecip") { if (reader.Read()) { lstsnowPrecip.Add(reader.Value.Trim()); } }
                                    if (reader.Name == "convPrecip") { if (reader.Read()) { lstconvPrecip.Add(reader.Value.Trim()); } }
                                    if (reader.Name == "rain") { if (reader.Read()) { lstrain.Add(reader.Value.Trim()); } }
                                    if (reader.Name == "snow") { if (reader.Read()) { lstsnow.Add(reader.Value.Trim()); } }
                                    if (reader.Name == "pressure") { if (reader.Read()) { lstpressure.Add(reader.Value.Trim()); } }
                                    if (reader.Name == "wind") { if (reader.Read()) { lstwind.Add(reader.Value.Trim()); } }
                                    if (reader.Name == "windDir") { if (reader.Read()) { lstwindDir.Add(reader.Value.Trim()); } }
                                    if (reader.Name == "rh") { if (reader.Read()) { lstrh.Add(reader.Value.Trim()); } }
                                    if (reader.Name == "gust") { if (reader.Read()) { lstgust.Add(reader.Value.Trim()); } }
                                    if (reader.Name == "waves") { if (reader.Read()) { lstwaves.Add(reader.Value.Trim()); } }
                                    if (reader.Name == "wavesDir") { if (reader.Read()) { lstwavesDir.Add(reader.Value.Trim()); } }
                                    if (reader.Name == "wavesPeriod") { if (reader.Read()) { lstwavesPeriod.Add(reader.Value.Trim()); } }
                                    if (reader.Name == "swell1") { if (reader.Read()) { lstswell1.Add(reader.Value.Trim()); } }
                                    if (reader.Name == "swell1Dir") { if (reader.Read()) { lstswell1Dir.Add(reader.Value.Trim()); } }
                                    if (reader.Name == "swell1Period") { if (reader.Read()) { lstswell1Period.Add(reader.Value.Trim()); } }
                                    if (reader.Name == "swell2") { if (reader.Read()) { lstswell2.Add(reader.Value.Trim()); } }
                                    if (reader.Name == "swell2Dir") { if (reader.Read()) { lstswell2Dir.Add(reader.Value.Trim()); } }
                                    if (reader.Name == "swell2Period") { if (reader.Read()) { lstswell2Period.Add(reader.Value.Trim()); } }
                                    if (reader.Name == "swell") { if (reader.Read()) { lstswell.Add(reader.Value.Trim()); } }
                                    if (reader.Name == "swellDir") { if (reader.Read()) { lstswellDir.Add(reader.Value.Trim()); } }
                                    if (reader.Name == "swellPeriod") { if (reader.Read()) { lstswellPeriod.Add(reader.Value.Trim()); } }



                                }
                            }
                        }



                        ////////////////////////////////////////

                        ////////////////////////////////////////
                    }



                    int intCount = 0;
                    intCount = lstday.Count;



                    for (int i = 0; i < intCount; i++)
                    {
                        strDataQuery = intHeaderPKey.ToString().Trim();
                        strDataQuery += ",'" + String.Format("{0:dd-MMM-yyyy HH:mm:ss}", Convert.ToDateTime(lstday[i].ToString())) + "'";
                        strDataQuery += ",'" + lsthour[i].ToString() + "'";
                        strDataQuery += ",'" + lstts[i].ToString() + "'";
                        strDataQuery += ",'" + lstorigTs[i].ToString() + "'";
                        strDataQuery += ",'" + lstisDay[i].ToString() + "'";
                        strDataQuery += ",'" + String.Format("{0:dd-MMM-yyyy HH:mm:ss}", Convert.ToDateTime(lstorigDate[i].ToString())) + "'";
                        strDataQuery += ",'" + lsttemp[i].ToString() + "'";
                        strDataQuery += ",'" + lstdewPoint[i].ToString() + "'";
                        strDataQuery += ",'" + lstcbase[i].ToString() + "'";
                        strDataQuery += ",'" + lsticon[i].ToString() + "'";
                        strDataQuery += ",'" + lsticon2[i].ToString() + "'";
                        strDataQuery += ",'" + lstweathercode[i].ToString() + "'";
                        strDataQuery += ",'" + lstmm[i].ToString() + "'";
                        strDataQuery += ",'" + lstsnowPrecip[i].ToString() + "'";
                        strDataQuery += ",'" + lstconvPrecip[i].ToString() + "'";
                        strDataQuery += ",'" + lstrain[i].ToString() + "'";
                        strDataQuery += ",'" + lstsnow[i].ToString() + "'";
                        strDataQuery += ",'" + lstpressure[i].ToString() + "'";
                        strDataQuery += ",'" + lstwind[i].ToString() + "'";
                        strDataQuery += ",'" + lstwindDir[i].ToString() + "'";
                        strDataQuery += ",'" + lstrh[i].ToString() + "'";
                        strDataQuery += ",'" + lstgust[i].ToString() + "'";
                        strDataQuery += ",'" + lstwaves[i].ToString() + "'";
                        strDataQuery += ",'" + lstwavesDir[i].ToString() + "'";
                        strDataQuery += ",'" + lstwavesPeriod[i].ToString() + "'";
                        strDataQuery += ",'" + lstswell1[i].ToString() + "'";
                        strDataQuery += ",'" + lstswell1Dir[i].ToString() + "'";
                        strDataQuery += ",'" + lstswell1Period[i].ToString() + "'";
                        strDataQuery += ",'" + lstswell2[i].ToString() + "'";
                        strDataQuery += ",'" + lstswell2Dir[i].ToString() + "'";
                        strDataQuery += ",'" + lstswell2Period[i].ToString() + "'";
                        strDataQuery += ",'" + lstswell[i].ToString() + "'";
                        strDataQuery += ",'" + lstswellDir[i].ToString() + "'";
                        strDataQuery += ",'" + lstswellPeriod[i].ToString() + "'";


                        QueryExecute(strDataQueryPattern + strDataQuery + " ,0,'System',GetDate(),'System',GetDate()) ");



                    }





                }


                /////////////////////////////////////

                //GET CURRENT DATA CSV

                //ParseCSV(intShipId, dtUtcTime, strLat, strLong);



            }



                // File.WriteAllText("D:\\" + strShipId + "_WeatherInfo.txt", strMainString);



            catch (System.Xml.XmlException e)
            {
                strOutput = e.Message;
            }
            catch (Exception e)
            {
                strOutput = e.Message;
            }

            return strOutput;
        }

        private void ParseCSV(int intShipId, DateTime dtUtcTime, string strLat, string strLong)
        {
            string strInputString = String.Empty;

            strInputString += "https://api.planetos.com/v1/datasets/nasa_oscar_global_5day/point?origin=dataset-details&lat=" + strLat + "&lon=" + strLong + "&apikey=af8c143a65eb4d52b41029d8428b4021&csv=true&count=50";

            try
            {


                // Create a request for the URL.   
                WebRequest request = WebRequest.Create(strInputString);
                // If required by the server, set the credentials.  
                request.Credentials = CredentialCache.DefaultCredentials;
                // Get the response.  
                WebResponse response = request.GetResponse();
                // Display the status.  
                Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                // Get the stream containing content returned by the server.  
                Stream dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.  
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.  
                string responseFromServer = reader.ReadToEnd();
                // Display the content.  
                // Console.WriteLine(responseFromServer);

                //tbOutput.Text = responseFromServer;

                // Clean up the streams and the response.  
                reader.Close();
                response.Close();



                SaveSeaCurrentInfo(responseFromServer, intShipId, dtUtcTime);

            }
            catch (WebException e)
            {

            }
            catch (Exception e)
            {

            }
        }


        private void SaveSeaCurrentInfo(string responseFromServer, int intShipId, DateTime dtUtcTime)
        {

            string strBuild = String.Empty;


            string strSep = "\n";
            char[] Separator = strSep.ToCharArray();
            string[] arrLines = responseFromServer.Split(Separator);

            int intCount = 0;
            foreach (string arrLine in arrLines)
            {
                intCount += 1;


                if (intCount > 1)
                {
                    string commaSep = ",";
                    char[] commaSeparator = commaSep.ToCharArray();
                    string[] arrLines2 = arrLine.Split(commaSeparator);

                    if (arrLines2.Length > 1)
                    {

                        string strUtcDateTime = String.Empty;

                        strUtcDateTime = String.Format("{0:dd-MMM-yyyy HH:mm:ss}", dtUtcTime);

                        string strCurrentDataQuery = String.Empty;
                        strCurrentDataQuery += " INSERT INTO  WeatherSeaCurrentData2(ShipId,UTC_DateTime,latitude,longitude,time_info,z, ";
                        strCurrentDataQuery += " u_Ocean_Surface_Zonal_Currents_m_per_sec,um_Ocean_Surface_Zonal_Currents_Maximum_Mask_m_per_sec, ";
                        strCurrentDataQuery += " v_Ocean_Surface_Meridional_Currents_m_per_sec,vm_Ocean_Surface_Meridional_Currents_Maximum_Mask_m_per_sec, ";
                        strCurrentDataQuery += " VEEMS_DELETED_FLAG,VEEMS_CREATED_BY,VEEMS_CREATED_DATE,VEEMS_MODIFIED_BY,VEEMS_MODIFIED_DATE) ";
                        strCurrentDataQuery += " VALUES ";
                        strCurrentDataQuery += " (" + intShipId.ToString().Trim() + ",'" + strUtcDateTime + "','" + arrLines2[0].Trim() + "','" + arrLines2[1].Trim() + "','" + String.Format("{0:dd-MMM-yyyy HH:mm:ss}", Convert.ToDateTime(arrLines2[2].Trim().Replace("\"", ""))) + "','" + arrLines2[3].Trim() + "','" + arrLines2[4].Trim() + "','" + arrLines2[5].Trim() + "','" + arrLines2[6].Trim() + "','" + arrLines2[7].Trim() + "' ";
                        strCurrentDataQuery += " ,0,'System',GetDate(),'System',GetDate()) ";

                        QueryExecute(strCurrentDataQuery);

                    }


                }




            }




        }


        private string QuerySelectRetString(string strQuery)
        {
            string strOutput = String.Empty;

            DataSet dsResult = new DataSet();
            VEEMSOffice.Components.Business.GenBO ParamGenBO = new VEEMSOffice.Components.Business.GenBO();

            dsResult = ParamGenBO.SelectDynQuery(strQuery);

            if ((dsResult != null) && (dsResult.Tables[0].Rows.Count > 0))
            {
                strOutput = dsResult.Tables[0].Rows[0][0].ToString().Trim();
            }

            return strOutput;

        }


        private int QuerySelectRetInt(string strQuery)
        {
            int intOutput = 0;

            DataSet dsResult = new DataSet();
            VEEMSOffice.Components.Business.GenBO ParamGenBO = new VEEMSOffice.Components.Business.GenBO();

            dsResult = ParamGenBO.SelectDynQuery(strQuery);

            if ((dsResult != null) && (dsResult.Tables[0].Rows.Count > 0))
            {
                intOutput = Convert.ToInt32(dsResult.Tables[0].Rows[0][0].ToString().Trim());
            }

            return intOutput;

        }


        private DataSet QuerySelect(string strQuery)
        {

            DataSet dsResult = new DataSet();
            VEEMSOffice.Components.Business.GenBO ParamGenBO = new VEEMSOffice.Components.Business.GenBO();

            dsResult = ParamGenBO.SelectDynQuery(strQuery);

            return dsResult;

        }

        private int QueryExecute(string strQuery)
        {

            int intResult = 0;
            VEEMSOffice.Components.Business.GenBO ParamGenBO = new VEEMSOffice.Components.Business.GenBO();

            intResult = ParamGenBO.ExecuteDynSP(strQuery);


            return intResult;

        }



    }
}
